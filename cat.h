///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 03b - Animal Farm 1
///
/// @file cat.h
/// @version 1.0
///
/// Exports data about cats
///
/// @author @todo yourName <@todo yourMail@hawaii.edu>
/// @brief  Lab 03b - AnimalFarm1 - EE 205 - Spr 2021
/// @date   @todo dd_mmm_yyyy
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <stdbool.h>

#include "animals.h"

enum CatBreed { MAIN_COON, MANX, SHORTHAIR, PERSIAN, SPHYNX };

struct Cat {
  char          name[32];
  enum Gender   gender;
  enum CatBreed breed;
  bool          isFixed;
  float         weight;
  enum Color    collar1_color;
  enum Color    collar2_color;
  long          license;
};

/// Other source files may want to use our cat database
extern struct Cat catDB[ MAX_SPECIES ];


/// Add Alice to the Cat catabase at position i.
/// 
/// @param int i Where in the catDB array that Alice the cat will go
///
void addAliceTheCat( int i ) ;


/// Print a cat from the database
/// 
/// @param int i Which cat in the database that should be printed
///
void printCat( int i ) ;


/// Return a string for the name of the color
char* breedName (enum CatBreed breed);

