###############################################################################
# University of Hawaii, College of Engineering
# EE 205  - Object Oriented Programming
# Lab 03b - Animal Farm 1
#
# @file    Makefile
# @version 1.0
#
# @author @todo yourName <@todo yourMail@hawaii.edu>
# @brief  Lab 03b - Animal Farm 1 - EE 205 - Spr 2021
# @date   @todo dd_mmm_yyyy
###############################################################################

all: animalfarm

cat.o: cat.c cat.h animals.h
	gcc -c cat.c

main.o: main.c cat.h
	gcc -c main.c

animals.o: animals.c animals.h
	gcc -c animals.c

animalfarm: cat.o main.o animals.o
	gcc -o animalfarm cat.o main.o animals.o

clean:
	rm -f *.o animalfarm
